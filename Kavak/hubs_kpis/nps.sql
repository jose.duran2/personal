create table playground.dl_catalog_nps as (
with sales_clients as (
  select 
    fb.bk_booking as estimate, 
    dc.bk_client as client_id_s, 
    dc.bk_client_olimpo as olimpo_id_s 
  from 
    dwh.fact_booking fb 
    left join dwh.dim_client dc on fb.fk_client = dc.sk_client 
    left join dwh.dim_geography dg on fb.fk_client_geo = dg.sk_geography 
    left join dwh.dim_location dl on dl.sk_location = fb.fk_car_booking_location
  where 
    dg.country_iso = 'MX'
)
, nps as(
  select 
    dd."date" as transaction_date
    , date(fnr.fk_answer_date) as answer_date
   , dntt.transaction_type_description
    ,(
      case 
	      when dntt.transaction_type_description = 'Aftersales 3M' then 'Aftersales 3M' 
	      when dntt.transaction_type_description = 'Seller' then 'Seller'
	      when dntt.transaction_type_description = 'Buyer Cash' 
      or dntt.transaction_type_description = 'Buyer Financed' then 'Buyer' end
    ) as nps_type, 
    dnsc.nps_score_category_description as nps_score, 
    fnr.bk_stock_id,  
    fnr.bk_transaction, 
    case 
	    when dntt.transaction_type_description = 'Aftersales 3M' 
    or dntt.transaction_type_description = 'Buyer Cash' 
    or dntt.transaction_type_description = 'Buyer Financed' then sales_clients.client_id_s 
    else null 
    end as client_id_sales, 
    case 
	    when dntt.transaction_type_description = 'Aftersales 3M' 
    or dntt.transaction_type_description = 'Buyer Cash' 
    or dntt.transaction_type_description = 'Buyer Financed' then sales_clients.olimpo_id_s 
    else null 
    end as olimpo_id_sales ,
    fnr.flag_validity
  from 
    dwh.fact_nps_response fnr 
    left join dwh.dim_geography dg on fnr.fk_geography = dg.sk_geography 
    left join dwh.dim_date dd on fnr.fk_transaction_date = dd.sk_date 
    left join dwh.dim_nps_score_category dnsc on fnr.fk_nps_score_category = dnsc.sk_nps_score_category 
    left join dwh.dim_nps_transaction_type dntt on fnr.fk_nps_transaction_type = dntt.sk_transaction_type 
    left join sales_clients on fnr.bk_transaction = sales_clients.estimate 
  where 
    dg.country_iso = 'MX' 
    and dd."date" >= '2023-01-01' 
    and dd."date" <= '2023-08-07' 
    and nps_type in ('Aftersales 3M', 'Buyer', 'Seller')
    and flag_validity is true
    ) 
, purchase_clients as(
  select 
    spr.transaction_id as purchase_transaction_id, 
    spr.customer_id as purchase_client_id, 
    dc2.bk_client_olimpo as purchase_olimpo_id 
  from 
    stg.stg_pur_registers spr 
    left join dwh.dim_client dc2 on spr.customer_id = dc2.bk_client 
  where 
    spr.country = 'MX' 
    and dc2.is_active = true
    and spr.item_receipt_date is not null
), 
nps_with_purchase_clients as(
select 
transaction_date,
answer_date,
transaction_type_description, 
nps_type,
nps_score,
bk_stock_id,
bk_transaction,
client_id_sales,
olimpo_id_sales,
flag_validity,
case when nps_type = 'Seller' then purchase_clients.purchase_client_id 
else null end as client_id_purchases,
case when nps_type = 'Seller' then purchase_clients.purchase_olimpo_id  
else null end as olimpo_id_purchases
from nps
left join purchase_clients on bk_transaction = purchase_clients.purchase_transaction_id 
) ,
nps_with_olimpo_id as(
select 
transaction_date,
answer_date,
transaction_type_description, 
nps_type,
nps_score,
bk_stock_id,
bk_transaction,
client_id_sales,
olimpo_id_sales,
client_id_purchases,
olimpo_id_purchases,
flag_validity,
case 
	when nps_type = 'Buyer' or nps_type= 'Aftersales 3M' then client_id_sales  
	when nps_type= 'Seller' then client_id_purchases 
else null 
end as client_id_final,
case 
	when nps_type = 'Buyer' or nps_type= 'Aftersales 3M' then olimpo_id_sales  
	when nps_type= 'Seller' then olimpo_id_purchases
else null 
end as olimpo_id_final
from nps_with_purchase_clients
),
hubs as (
select uha.identifier , hub, date(date_rules) as date_rules , olimpo_id
from prometheus_global_landing.users_hub_assignation uha
left join prometheus_global_landing.user_olimpo_merged uom
on uha.identifier = uom.identifier  --4168661
)
,nps_with_hub as (
select 
transaction_date,
answer_date, 
transaction_type_description, 
olimpo_id_final,
nps_score,
nps_type,
bk_stock_id,
bk_transaction,
client_id_final,
flag_validity,
coalesce(hubs.hub, hubs2.hub, 'Sin asignar') as customer_hub
from 
  nps_with_olimpo_id 
  left join 
    ( select h1.olimpo_id, h1.date_rules, h1.hub,
            ROW_NUMBER() OVER (PARTITION BY h1.olimpo_id ORDER BY h1.date_rules DESC) as rn
        from hubs h1
        inner join nps_with_olimpo_id 
        on h1.olimpo_id = nps_with_olimpo_id.olimpo_id_final
        where  h1.date_rules<=nps_with_olimpo_id.transaction_date
    ) hubs ON nps_with_olimpo_id.olimpo_id_final = hubs.olimpo_id AND hubs.rn = 1
  left join (select h2.olimpo_id, h2.date_rules, h2.hub,
  				row_number() over (partition by h2.olimpo_id order by h2.date_rules desc) as rn
  			from hubs h2
  			inner join nps_with_olimpo_id on h2.olimpo_id = nps_with_olimpo_id.olimpo_id_final
  			) hubs2 on nps_with_olimpo_id.olimpo_id_final = hubs2.olimpo_id and hubs2.rn = 1
  ), nps_latest as (
  select nps_with_hub.*
  , coalesce(dlch.region, 'Sin asignar') as customer_region
  from nps_with_hub 
  left join playground.dl_catalog_hubs dlch 
 on lower(nps_with_hub.customer_hub) = lower(dlch.raw_hub))
 , final_table as (
select distinct
transaction_date, answer_date, transaction_type_description, client_id_final as customer_id 
, olimpo_id_final as olimpo_id, nps_score, nps_type, customer_hub, customer_region, bk_stock_id
from nps_latest )
select * from final_table);