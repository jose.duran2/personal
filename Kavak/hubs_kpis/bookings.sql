create table playground.dl_catalog_inv_to_inv as (
with 
dates as (select 20230101 as date_parameter )
, ready_to_sell as (
             select 
            dd.full_date as inv_date
			,fi.country_iso
			,fk_car_stock
            ,(case when dis.inventory_status_desc in ('available', 'booked') then 1 else 0 end) as ready_to_sell
            --,count(fi.bk_stock) total_stock
            ,SUM(ready_to_sell) OVER(PARTITION BY 1 ) AS total_published_days
            ,SUM(ready_to_sell) OVER(PARTITION BY 1 ORDER BY inv_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as published_days_to_date
            ,SUM(ready_to_sell) OVER(PARTITION BY 1 ORDER BY inv_date ROWS BETWEEN 29 PRECEDING AND CURRENT ROW) AS published_days_last_30_days
            ,MIN(inv_date) OVER(PARTITION BY fk_car_stock) AS first_published_day
            ,MAX(inv_date) OVER(PARTITION BY fk_car_stock) AS last_published_day 
          from dwh.fact_inventory fi
          	inner join dwh.dim_inventory_status dis on fi.fk_olp_car_inventory_status = dis.sk_inventory_status
          	inner join dwh.dim_date dd on dd.sk_date = fi.fk_inventory_date 
          where 1=1
            and fi.flg_olp_stock=1
           -- and dd.full_date ='2022-12-12'
            and fi.country_iso = 'MX'
            and qty_nts_on_hand_finance = 1
            --and fk_car_stock = 1330492--(1277852)
            order by 1 desc       
)
,inventory as (
--create table playground.LA_inventory_raw as(
             select 
            dd.full_date as inv_date
			,fi.country_iso
			,dd.day_is_first_of_month
			,dd.day_is_last_of_month
			,bk_stock
			,bk_sku
			,fk_car_sku
			,fi.fk_car_stock
			,fk_olp_hub_location -- este es el que usamos en la query gral
			,dl1.hub_name 
			,fk_nts_location
			,dl2.hub_name as hub_name_ns
			,nts_is_crab_car
			,flg_inventory
			,fk_olp_published_date
            ,(case when dis.inventory_status_desc in ('available', 'booked') then 1 else 0 end) as ready_to_sell
			,(case when dis.inventory_status_desc in ('available', 'booked', 'preloaded','preloadedBooked', 'preloadedFakeBooked','fakeBooked') 
					then 1 else 0 end) as total_inventory
            --,count(fi.bk_stock) total_stock
			,rts.total_published_days,rts.published_days_to_date,rts.published_days_last_30_days,rts.first_published_day,rts.last_published_day
          from dwh.fact_inventory fi
          inner join dwh.dim_inventory_status dis on fi.fk_olp_car_inventory_status = dis.sk_inventory_status 
          inner join dwh.dim_date dd on dd.sk_date = fi.fk_inventory_date 
          left join dwh.dim_location dl1 on dl1.sk_location  = fk_olp_hub_location
          left join dwh.dim_location dl2 on dl2.sk_location = fk_nts_location
          left join ready_to_sell as rts on rts.fk_car_stock=fi.fk_car_stock and  dd.full_date=rts.inv_date
          left join dates on 1=1
          where 1=1
            and fi.flg_olp_stock=1
            and dd.full_date >= date(dates.date_parameter)-- '2023-01-01'
            and (dd.day_is_first_of_month=1 or dd.week_day_number=1) --selecciono el primer dia dle mes y el primer dia de la semana
          --and dd.full_date ='2022-12-12'
            and fi.country_iso in ('MX')
            and qty_nts_on_hand_finance = 1
            --and flg_inventory <> total_inventory
            --and to_char(dd.full_date,'YYYY-MM')= '2023-01'
)select * from inventory);--550103
	
--Crear tabla de inv_base para joinear al final
create table playground.dl_catalog_inv_base as (
select distinct inv_date, bk_stock ,fk_car_stock from playground.dl_catalog_inv_to_inv);

select inv_date, count(*) from playground.dl_catalog_inv_base group by 1 order by 1;

------ Bookings
/*create table playground.dl_catalog_bookings_to_inv as (
with bookings as (
select ir.inv_date,ir.fk_car_stock
,count(distinct fb.bk_booking) roll_30D_booking
,count(distinct fbm.bk_booking) roll_30D_main_booking
from playground.dl_catalog_inv_base as ir
-- from playground.LA_inventory_raw ir
	left join dwh.fact_booking fb on fb.fk_stock_id = ir.fk_car_stock and (fb.fk_booking_creation_date between ir.inv_date - 30 and ir.inv_date)
	left join dwh.fact_booking fbm on fb.fk_stock_id = ir.fk_car_stock and (fb.fk_booking_creation_date between ir.inv_date - 30 and ir.inv_date and fbm.flag_main_booking is true) 
	group by 1,2)
select * from bookings;*/

create table playground.dl_catalog_bookings_to_inv as (
select ir.inv_date,ir.fk_car_stock
,count(distinct fb.bk_booking) roll_30D_booking
from playground.dl_catalog_inv_base as ir
	left join dwh.fact_booking fb 
	on fb.fk_stock_id = ir.fk_car_stock 
	and (fb.fk_booking_creation_date between ir.inv_date - 30 and ir.inv_date)
group by 1,2);--550103

create table playground.dl_catalog_main_bookings_to_inv as (
select ir.inv_date,ir.fk_car_stock
,count(distinct fbm.bk_booking) roll_30D_main_booking
from playground.dl_catalog_inv_base as ir
	left join dwh.fact_booking fbm 
	on fbm.fk_stock_id = ir.fk_car_stock and 
	(fbm.fk_booking_creation_date between ir.inv_date - 30 and ir.inv_date 
	and fbm.flag_main_booking is true)
group by 1,2);--550103

------ Leads
create table playground.dl_catalog_leads_to_inv as (
select 
ir.inv_date,ir.fk_car_stock
--,sl.desc_last_channel--,sk_lead,fk_user 
,count(distinct sl.sk_lead) roll_30D_lead_lead
,count(distinct sl.fk_user) roll_30D_lead_user
,count(distinct case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete)  then sl.sk_lead end) roll_30D_lead_complete_lead
,count(distinct case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete)  then sl.fk_user end) roll_30D_lead_complete_user
,count(distinct case when sl.desc_last_channel = 'APPOINTMENT' then sl.sk_lead end ) roll_30D_lead_appoiment_lead
,count(distinct case when sl.desc_last_channel = 'FINANCING' then sl.sk_lead end ) roll_30D_lead_fin_lead
,count(distinct case when sl.desc_last_channel = 'CHECKOUT' then sl.sk_lead end ) roll_30D_lead_chk_lead
,count(distinct case when sl.desc_last_channel = 'APPOINTMENT' then sl.fk_user end ) roll_30D_lead_appoiment_user
,count(distinct case when sl.desc_last_channel = 'FINANCING' then sl.fk_user end ) roll_30D_lead_fin_user
,count(distinct case when sl.desc_last_channel = 'CHECKOUT' then sl.fk_user end ) roll_30D_lead_chk_user
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'APPOINTMENT'
		then sl.sk_lead end) roll_30D_lead_appoiment_complete_lead
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'FINANCING'
		then sl.sk_lead end) roll_30D_lead_fin_complete_lead
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'CHECKOUT'
		then sl.sk_lead end) roll_30D_lead_chk_complete_lead
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'APPOINTMENT'
		then sl.fk_user end) roll_30D_lead_appoiment_complete_user
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'FINANCING'
		then sl.fk_user end) roll_30D_lead_fin_complete_user
,count(distinct 
	case when (sl.flag_appointment_complete or sl.flag_financing_complete or sl.flag_checkout_complete) and sl.desc_last_channel = 'CHECKOUT'
		then sl.fk_user end) roll_30D_lead_chk_complete_user
----DIFF
,roll_30d_lead_user-(roll_30d_lead_appoiment_user+roll_30d_lead_fin_user+roll_30d_lead_chk_user) diff_user
,roll_30d_lead_complete_user-(roll_30d_lead_appoiment_complete_user+roll_30d_lead_fin_complete_user+roll_30d_lead_chk_complete_user) diff_complete_user
from playground.dl_catalog_inv_base ir
--from playground.LA_inventory_raw ir
	left join dwh.sales_lead sl on 
		coalesce (fk_last_auto_appointment,fk_last_auto_checkout,fk_last_auto_financing) = ir.fk_car_stock and sl.lead_end_date between ir.inv_date - 30 and ir.inv_date
where 1=1
--and coalesce (fk_last_auto_appointment,fk_last_auto_checkout,fk_last_auto_financing) = 1358177
group by 1,2--,3
--having (diff_user!=0 or diff_complete_user!=0)
);

--select count(*) from playground.dl_catalog_leads_to_inv;

------ VIPS
create table playground.dl_catalog_vips_to_inv as (
with v as (
select DATE(event_time_mx_city) as date_vip
, car_id
, merged_amplitude_id
from serving.amplitude_vip_viewed_mx)
, vips_table as (
 select 
ir.inv_date,ir.fk_car_stock
, count(distinct v.merged_amplitude_id) as vips
from playground.dl_catalog_inv_base ir
	left join v
	on ir.bk_stock= v.car_id 
	and date_vip between ir.inv_date - 30 and ir.inv_date
where 1=1
group by 1,2)
select inv_date, fk_car_stock, vips from vips_table
);

--select * from playground.dl_catalog_vips_to_inv limit 10;

create table playground.dl_catalog_inspection_to_inv as (
select 
ir.inv_date
,ir.fk_car_stock
,dd.full_date inspection_made_date
,dd2.full_date item_receipt_date
--,cs.bk_car_stock
,fk_kavak_location
,dl.hub_name
,coalesce (dl.location_name, 'Client') as hub_inspection_name
,dl2.location_name as hub_reception_name --donde se entregó el auto
,r.flag_purchase_b2b 
from playground.dl_catalog_inv_base ir
	left join dwh.fact_registers r on ir.fk_car_stock=r.fk_car_stock and flag_gross_purchases = 1 and fk_item_receipt_date is not null
	--left join dwh.dim_car_stock cs on cs.sk_car_stock = r.fk_car_stock 
	left join dwh.dim_location dl on dl.sk_location  = r.fk_mec_inspection_location --donde se inspecciona
	left join dwh.dim_location dl2 on dl2.sk_location  = r.fk_kavak_location -- hub donde se entrega el auto
	left join dwh.dim_date dd on dd.sk_date =r.fk_inspection_made_date
	left join dwh.dim_date dd2 on dd2.sk_date =r.fk_item_receipt_date
where 1=1
and country='MX'
);

create table playground.dl_catalog_bookings_complete as (
with inv_final as (
select 
-- * 
ir.inv_date
,ir.country_iso
,ir.bk_stock
,ir.bk_sku
,ir.fk_car_sku
,ir.fk_car_stock
,ir.fk_olp_hub_location
,ir.hub_name
,ir.fk_nts_location
,ir.hub_name_ns
,ir.nts_is_crab_car
,ir.fk_olp_published_date
--,ir.published_days_last_30_days
--,ir.less_30d_published
,ir.ready_to_sell
,ir.total_inventory
,b.roll_30d_booking
,mb.roll_30d_main_booking
,l.roll_30d_lead_lead
,l.roll_30d_lead_complete_lead
,l.roll_30d_lead_appoiment_lead
,l.roll_30d_lead_fin_lead
,l.roll_30d_lead_chk_lead
,l.roll_30d_lead_appoiment_complete_lead
,l.roll_30d_lead_fin_complete_lead
,l.roll_30d_lead_chk_complete_lead
,v.vips
,i.item_receipt_date
,i.hub_inspection_name
,i.hub_reception_name
,ir.total_published_days
,ir.published_days_to_date
,ir.published_days_last_30_days
,ir.first_published_day,ir.last_published_day
, coalesce(ir.hub_name, ir.hub_name_ns, 'Sin asignar') as stock_hub
from playground.dl_catalog_inv_to_inv ir
--from playground.LA_inventory_raw ir
	left join playground.dl_catalog_bookings_to_inv b on b.inv_date = ir.inv_date and b.fk_car_stock=ir.fk_car_stock
	left join playground.dl_catalog_main_bookings_to_inv mb on mb.inv_date = ir.inv_date and mb.fk_car_stock=ir.fk_car_stock
	left join playground.dl_catalog_leads_to_inv l on l.inv_date = ir.inv_date and l.fk_car_stock=ir.fk_car_stock
	left join playground.dl_catalog_vips_to_inv  v on v.inv_date = ir.inv_date and v.fk_car_stock=ir.fk_car_stock
	left join playground.dl_catalog_inspection_to_inv i on i.inv_date = ir.inv_date and i.fk_car_stock=ir.fk_car_stock
order by 1,2,3)
select distinct inv_final.*
, coalesce(hubs.region, 'Sin asignar') as stock_region
,hubs2.region as region_reception_name
,hubs1.region as region_inspection_name
, dcc.p_segment_final  
from inv_final 
	left join playground.dl_catalog_hubs hubs on lower(stock_hub) = lower(hubs.raw_hub)
	left join playground.dl_catalog_hubs hubs1 on lower(hub_inspection_name) = lower(hubs1.raw_hub)
	left join playground.dl_catalog_hubs hubs2 on lower(hub_reception_name) = lower(hubs2.raw_hub)
	left join playground.dl_catalog_categories dcc	on bk_sku = dcc.vrs_sku
);


grant select on playground.dl_catalog_bookings_complete to lucas_aimaretti;
grant select on playground.dl_catalog_bookings_complete to sofia_ortiz;
grant select on playground.dl_catalog_bookings_complete to milton_gomez;
grant select on playground.dl_catalog_bookings_complete to jorge_garcia;
grant select on playground.dl_catalog_bookings_complete to diego_alba;




