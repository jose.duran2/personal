create table playground.dl_catalog_registers as (
with registers as (
	WITH tmp_variables AS
(
       SELECT '2023-01-01'::date AS start_date,
              '2023-08-09'::date AS end_date)
SELECT    
	fr.register_id
	, 1 AS flag_registers
    , dd."date" as registers_creation_date
    , case
	    WHEN fr.flag_scheduled = true and fr.fk_inspection_creation_date is not null then 1 else 0
	  end as flag_scheduled_inspections
	, dd2."date"  AS scheduled_inspections_date
    , case
	    when fr.flag_made = true and fr.fk_inspection_start_date is not null
	    and dr.result_status in ('approved', 'rejected') then 1 else 0
      end as flag_made_inspections
    , case 
	    when fr.flag_made = true
	    and fr.fk_inspection_start_date is not null
	    and dr.result_status in ('approved', 'rejected') then dd3."date"
        else null
      end as  made_inspections_date
    , case 
	    when fr.flag_approved = true and fr.fk_inspection_start_date is not null then 1 else 0
      end as flag_approved_inspections
    , case
	    when fr.flag_approved = true and fr.fk_inspection_start_date is not null then dd3."date"
        else null
      end as approved_inspections_date
    , case 
	    when fr.flag_purchase = true and fr.fk_item_receipt_date is not null then 1 else 0
      end as flag_purchases
    , dd4."date"  AS purchases_receipt_date
    , dc.bk_client  as client_id
    , dc.bk_client_olimpo as olimpo_id
    , fr.sku
    , dl.location_name  as inspection_hub
    , hubs.hub_std as inspection_hub_std
    , hubs.region as inspection_region_std
from  dwh.fact_registers fr
left join dwh.dim_result dr on dr.sk_result = fk_inspection_result
left join dwh.dim_date dd on fr.fk_register_creation_date = dd.sk_date 
left join dwh.dim_date dd2 on fr.fk_inspection_creation_date  = dd2.sk_date
left join dwh.dim_date dd3 on fr.fk_inspection_start_date  = dd3.sk_date
left join dwh.dim_date dd4 on fr.fk_item_receipt_date  = dd4.sk_date
left join dwh.dim_location dl on fr.fk_mec_inspection_location = dl.sk_location 
left join dwh.dim_client dc on fr.fk_client = dc.sk_client 
left join playground.dl_catalog_hubs hubs on lower(dl.location_name) = lower(hubs.raw_hub) 
where fr.source = 'salesforce'
and fr.country = 'MX'
and fr.is_test = 0
and (dd."date" >=(select start_date from tmp_variables)
    or dd2."date" >=(select start_date from tmp_variables)
    or dd3."date" >=(select start_date from tmp_variables)
    or dd4."date" >=(select start_date from tmp_variables))
    )--select count(*) from registers;--1367673
, hubs as (
select hub, date_rules, olimpo_id --as olimpo_id_hubs 
from prometheus_global_landing.users_hub_assignation uha
left join prometheus_global_landing.user_olimpo_merged uom
on uha.identifier = uom.identifier  
)
,registers_with_hub as (
select registers.*
    , coalesce(hubs1.hub, hubs2.hub, hubs3.hub) as customer_hub
    --, hubs1.region as customer_region
    from registers
	left join ( select h1.olimpo_id, h1.date_rules, h1.hub,
	            ROW_NUMBER() OVER (PARTITION BY h1.olimpo_id ORDER BY h1.date_rules DESC) as rn
	        from hubs h1
	        inner join registers on h1.olimpo_id = registers.olimpo_id
	        where h1.date_rules <= registers.registers_creation_date
	    ) hubs1 ON registers.olimpo_id = hubs1.olimpo_id AND hubs1.rn = 1    
	left join (select h2.olimpo_id, h2.date_rules, h2.hub,
	  				row_number() over (partition by h2.olimpo_id order by h2.date_rules desc) as rn
	  			from hubs h2
	  			inner join registers on h2.olimpo_id = registers.olimpo_id
	  			) hubs2 on registers.olimpo_id = hubs2.olimpo_id and hubs2.rn = 1	
	left join (select h3.olimpo_id, h3.date_rules, h3.hub,
	  				row_number() over (partition by h3.olimpo_id order by h3.date_rules asc) as rn
	  			from hubs h3
	  			inner join registers on h3.olimpo_id = registers.olimpo_id
	  			) hubs3 on registers.olimpo_id = hubs3.olimpo_id and hubs3.rn = 1	
    )
, assign_transaction_hub as (
		 select registers_with_hub.*
			, hubs3.region as customer_region
			, coalesce(inspection_hub_std, customer_hub, 'Sin asignar')as transaction_hub
		from registers_with_hub 
		left join playground.dl_catalog_hubs hubs3 
		on lower(registers_with_hub.customer_hub) = lower(hubs3.raw_hub) 
		) 
, final_registers as (--1367673 registers
		select distinct assign_transaction_hub.*
		, coalesce(hubs.region, 'Sin asignar') as transaction_region
		, coalesce(first_transaction.type_customer, 'New') as type_customer
		from assign_transaction_hub 
		left join playground.dl_catalog_hubs hubs 
		on lower(transaction_hub) = lower(hubs.raw_hub)
		left join
			(select distinct olimpo_id, 'Existing' as type_customer
			from prometheus_global_landing.first_transactions) first_transaction
			on assign_transaction_hub.olimpo_id = first_transaction.olimpo_id)
select final_registers.*
, case 
	when type_customer = 'New' then 1
	else 0
end as flag_customer_new
, case
	when type_customer = 'Existing' then 1
	else 0
end as flag_customer_existing
from final_registers);


grant all privileges on playground.dl_catalog_registers to jose_duran;
grant select privileges on playground.dl_catalog_registers to diego_alba;
grant all privileges on playground.dl_catalog_registers to sofia_ortiz;
grant all privileges on playground.dl_catalog_registers to milton_gomez;
grant all privileges on playground.dl_catalog_registers to jorge_garcia;