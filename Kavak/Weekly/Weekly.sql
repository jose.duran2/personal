-------------- SPEND
select 
start_date
,dd.week_day_number
,country_from_account_id
,c_funnel_label 
,trunc(sum(spend),0) as total_spend
from serving.ad_consolidation c
	inner join dwh.dim_date dd on dd.full_date = c.start_date
where start_date between '2022-01-01' and date(getdate())
and is_active = true
and country_from_account_id in ('MX','BR')
group by 1,2,3,4
having total_spend >0
order by 1,3

------- FULL
				--------------- PERIOD COUNTRY
with period_country as (
	select 
	full_date, country_iso,day_is_first_of_month,day_is_last_of_month,week_day_number,year_week_number,year_number,month_number 
	from dwh.dim_date dd
		cross join (select distinct country_iso from dwh.dim_geography) dg
	where dd.full_date between '2022-01-01' and date(getdate()) 
	and dg.country_iso in ('BR','MX','TR','AR','CL','CO','PE')
)
				--------------- SALES LEAD
, sales_lead as(
		select
			DATE(lead_end_date) AS full_date
			,dg.country_iso
			,COUNT(sk_lead) sales_leads
		from
			dwh.sales_lead sl 
			left join dwh.dim_geography dg on dg.sk_geography =sl.fk_geography
		where
			lead_end_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
		group by 1,2)
				--------------- BOOKINGS
, bookings_fin as(
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as bookings_fin
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_first_booking_creation_date 
			inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type='Financing'
		where 1=1
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
			and fb.flag_main_booking 
		group by 1,2)
, bookings_cash as(
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as bookings_cash
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_first_booking_creation_date 
			inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type!='Financing'
		where 1=1
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
			and fb.flag_main_booking 
		group by 1,2)
				--------------- SALES
,gross_sales as(
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as gross_sales
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_declared_sale_date
		where 1=1
			and fb.flag_declared_sale is true
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
		group by 1,2)

,cancelled_sales as (
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as cancelled_sales
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_sale_cancellation_date 
		where 1=1
		
			and fb.flag_declared_sale
			and fb.flag_declared_cancelled_sale  
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
		group by 1,2)
			--------------- RETURNED
, returned as(
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as total_returned
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_sale_cancellation_date 
		where 1=1
			--and fb.flag_declared_sale is true
			and dd.full_date between '2023-02-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
			--and fb.flag_main_booking
			--and fb.flag_returned
			and flag_declared_cancelled_sale IS TRUE
            and fk_sale_cancellation_date IS NOT null
            and fk_delivery_date IS NOT NULL

		group by 1,2)
		
,gross_sales_fin as(
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as gross_sales
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_declared_sale_date
			inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type='Financing'
		where 1=1
			and fb.flag_declared_sale is true
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
		group by 1,2)
		
,cancelled_sales_fin as (
		select
			dd.full_date
			,dg.country_iso
			,count(distinct fb.bk_booking) as cancelled_sales
		from
			dwh.fact_booking fb
			left join dwh.dim_geography dg on dg.sk_geography = fb.fk_client_geo
			left join dwh.dim_date dd on dd.sk_date = fb.fk_sale_cancellation_date
			inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type='Financing'
		
		where 1=1
			and fb.flag_declared_sale
			and fb.flag_declared_cancelled_sale  
			and dd.full_date between '2022-01-01' and date(getdate())
			and dg.country_iso in ('BR','MX','TR','AR')
		group by 1,2)

				--------------- PURCHASE & PURCHASE LEAD
,purchase as (
	select
		dd.full_date
		,prm.country as country_iso
		,SUM(prm.flag_register) purchase_lead--Lead
		,SUM(prm.flag_purchase) purchase--purchase
		,SUM(prm.flag_made) inspection_made--made
		,SUM(prm.flag_scheduled) inspection_scheduled --scheduled
		,SUM(prm.flag_approved) inspection_approved --approved
		--,SUM(case when flag_purchase_b2b = 0 then prm.flag_purchase else 0 end) purchase_c2b--purchase
		,cast(sum(flag_purchase) as int)-cast(sum(flag_purchase_b2b) as int) as purchase_c2b
	from dwh.pivot_registers_mv prm 
		 inner join dwh.dim_date dd on prm.fk_date =dd.sk_date 
	where 1=1
	and dd.full_date between '2022-01-01' and date(getdate())
	and country in ('BR','MX','TR','AR')
	group by 1,2
)
-------- Deliveries
,deliveries as (
		WITH qry1 AS (
		select	d.full_date,country_iso,count(*) gross_deliveries
		from dwh.fact_booking
			LEFT JOIN dwh.dim_date d on fk_scheduled_delivery_date = d.sk_date
			LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
			WHERE
		flag_gross_delivered IS TRUE
		AND country_iso = 'MX'
		GROUP by 1,2)
		,qry2 AS (
		select d.full_date,country_iso,count(*) "returns"
		from dwh.fact_booking
		LEFT JOIN dwh.dim_date d on fk_credit_memo_creation_date = d.sk_date
		LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
		
		WHERE
		flag_returned IS TRUE
		AND country_iso = 'MX'
		GROUP by 1,2)
SELECT
--to_char(qry1.full_date, 'YYYYMM') AS full_date
qry1.full_date, qry1.country_iso,sum(qry1.gross_deliveries) gross_delivery,sum(qry2."returns") "return",gross_delivery-"return" AS net_delivery
from qry1
LEFT JOIN qry2 on qry1.full_date = qry2.full_date AND qry1.country_iso = qry2.country_iso
GROUP by 1,2
--ORDER by 1 DESC
)
, deliveries_all as (
	
		WITH gross_delivery AS (
		select	d.full_date,country_iso,count(*) gross_deliveries
		from dwh.fact_booking fb
			LEFT JOIN dwh.dim_date d on fk_scheduled_delivery_date = d.sk_date
			LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
			where flag_gross_delivered IS true AND country_iso in ('MX','BR')
		GROUP by 1,2
		)
		,gross_delivery_f AS (
		select	d.full_date,country_iso,count(*) gross_deliveries_f
		from dwh.fact_booking fb
			LEFT JOIN dwh.dim_date d on fk_scheduled_delivery_date = d.sk_date
			LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
			inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type='Financing'
			WHERE
		flag_gross_delivered IS true AND country_iso in ('MX','BR')
		GROUP by 1,2
		)
		,returned AS (
		select d.full_date,country_iso,count(*) "returns"
		from dwh.fact_booking fb
		LEFT JOIN dwh.dim_date d on fk_credit_memo_creation_date = d.sk_date
		LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
		where flag_returned IS true AND country_iso in ('MX','BR')
		GROUP by 1,2)
		,returned_f AS (
		select d.full_date,country_iso,count(*) "returns_f"
		from dwh.fact_booking fb
		LEFT JOIN dwh.dim_date d on fk_credit_memo_creation_date = d.sk_date
		LEFT JOIN dwh.dim_geography on fk_client_geo = sk_geography
		inner join dwh.dim_payment_type o on fb.fk_payment_type= o.sk_payment_type and o.type='Financing'
		where flag_returned IS true AND country_iso in ('MX','BR')
		GROUP by 1,2)
		
SELECT
--to_char(gross_delivery.full_date, 'YYYYMM') AS full_date
gross_delivery.full_date
,gross_delivery.country_iso
,sum(gross_delivery.gross_deliveries) gross_delivery
,sum(returned."returns") "return",gross_delivery-"return" AS net_delivery
,sum(gross_delivery_f.gross_deliveries_f) gross_delivery_f
,sum(returned_f."returns_f") "return_f",gross_delivery_f-"return_f" AS net_delivery_f

from gross_delivery

LEFT JOIN gross_delivery_f on gross_delivery_f.full_date = gross_delivery.full_date AND gross_delivery_f.country_iso = gross_delivery.country_iso
LEFT JOIN returned on gross_delivery.full_date = returned.full_date AND gross_delivery.country_iso = returned.country_iso
LEFT JOIN returned_f on returned_f.full_date = gross_delivery.full_date AND returned_f.country_iso = gross_delivery.country_iso

--where to_char(gross_delivery.full_date, 'YYYYMM') = '202304'
GROUP by 1,2
	
)                   
-------- INVENTORY

,inventory as (
	with inv_br as (
             select 
             dd.full_date
            ,fi.country_iso
            ,sum(case when dis.inventory_status_desc in ('available', 'booked') then 1 else 0 end) as ready_to_sell
            ,sum(case when dis.inventory_status_desc <> 'sold' then 1 else 0 end) as total_inventory
            --,count(fi.bk_stock) total_stock
          from dwh.fact_inventory fi
          inner join dwh.dim_inventory_status dis on fi.fk_olp_car_inventory_status = dis.sk_inventory_status 
          inner join dwh.dim_date dd on dd.sk_date = fi.fk_inventory_date 
          where 1=1

            and fi.flg_olp_stock=1
            and dd.full_date >='2022-01-01'
          --and dd.full_date ='2022-12-12'
            and fi.country_iso in ('BR')
          group by 1,2
          order by 1,2
          )
	, inv_mx as (
	       
	       select 
	       dd.full_date
	       ,country_iso
			,sum(case when dis.inventory_status_desc in ('available', 'booked') then 1 else 0 end) as ready_to_sell
			,sum(case when dis.inventory_status_desc in ('available', 'booked', 'preloaded','preloadedBooked', 'preloadedFakeBooked','fakeBooked') 
					then 1 else 0 end) as total_inventory
			--,count(mi.bk_stock) total_stock
			from
			dwh.mv_inventory mi
			join dwh.dim_car_sku dcs on dcs.sk_car_sku = mi.fk_car_sku
			left join dwh.dim_inventory_status dis on mi.fk_olp_car_inventory_status =dis.sk_inventory_status
			left join dwh.dim_location dl on dl.sk_location = mi.fk_olp_hub_location
			inner join dwh.dim_date dd on dd.sk_date = mi.fk_inventory_date 
			where  1=1
			
			and dd.full_date >='2022-01-01'
			--and inventory_status_desc in ('available', 'booked', 'preloaded','preloadedBooked', 'preloadedFakeBooked','fakeBooked')
			and qty_nts_on_hand_finance = 1
			and country_iso = 'MX'
			group by 1,2
       )
       
       select * from inv_br
       union
       select * from inv_mx

   )
,nps as (
		
		with monthly_nps_unpivot AS (
		    --NPS Rate per transaction type
		    SELECT
		        e.country_iso,
		        a.year_number,
		        a.month_number,
		        a.month_name,
		        a.year_month,
		        CASE
		            WHEN d.transaction_type_description IN ('Buyer Financed', 'Buyer Cash', 'Buyer') THEN 'Buyer'
		            WHEN d.transaction_type_description IN ('Seller') THEN 'Seller'
		            WHEN d.transaction_type_description IN ('Aftersales 3M') THEN 'Aftersales'
		            WHEN d.transaction_type_description IN ('Seller Churn (AR)', 'Seller Churn (OR)') THEN 'Seller Churn'
		            ELSE 'Buyer Churn'
		        END AS transaction_group_type,
		        ROUND(
		            (
		                CAST(
		                    COUNT(
		                        CASE
		                            c.nps_score_category_description
		                            WHEN 'Promoter' THEN 1
		                        END
		                    ) - COUNT(
		                        CASE
		                            c.nps_score_category_description
		                            WHEN 'Detractor' THEN 1
		                        END
		                    ) AS FLOAT
		                ) / COUNT(
		                    CASE
		                        WHEN c.nps_score_category_description IN('Promoter', 'Detractor', 'Passive') THEN 1
		                    END
		                )
		            ) ,
		            2
		        ) AS nps_rate
		    FROM
		        dwh.dim_date a
		        LEFT JOIN dwh.fact_nps_response b ON (a.sk_date = b.fk_transaction_date)
		        LEFT JOIN dwh.dim_nps_score_category c ON (
		            b.fk_nps_score_category = c.sk_nps_score_category
		        )
		        LEFT JOIN dwh.dim_nps_transaction_type d ON (
		            b.fk_nps_transaction_type = d.sk_transaction_type
		        )
		        LEFT JOIN dwh.dim_geography e ON (b.fk_geography = e.sk_geography)
		    WHERE
		        b.flag_validity = TRUE
		        AND d.transaction_type_description IN (
		            'Seller',
		            'Buyer Financed',
		            'Buyer Cash',
		            'Aftersales 3M',
		            'Buyer Churn',
		            'Seller Churn (AR)',
		            'Seller Churn (OR)',
		            'Buyer'
		        )
		        AND d.is_active = TRUE
		        AND a.year_number >= 2022
		GROUP by 1,2,3,4,5,6
		),
		monthly_nps_pivot AS (
		    --NPS Rate per transaction type - as columns
		    SELECT
		        country_iso,
		        year_number,
		        month_number,
		        month_name,
		        year_month,
		        CASE
		            WHEN transaction_group_type = 'Buyer' THEN nps_rate
		        END AS nps_buyer,
		        CASE
		            WHEN transaction_group_type = 'Seller' THEN nps_rate
		        END AS nps_seller,
		        CASE
		            WHEN transaction_group_type = 'Aftersales' THEN nps_rate
		        END AS nps_aftersales,
		        CASE
		            WHEN transaction_group_type = 'Buyer Churn' THEN nps_rate
		        END AS nps_buyerchurn,
		        CASE
		            WHEN transaction_group_type = 'Seller Churn' THEN nps_rate
		        END AS nps_sellerchurn
		    FROM
		        monthly_nps_unpivot
		) --NPS metrics
		SELECT
		    country_iso,
		    CAST(year_month||'01' as date) as full_date ,
		    SUM(nps_buyer) AS nps_buyer,
		    SUM(nps_seller) AS nps_seller,
		    SUM(nps_aftersales) AS nps_aftersales,
		    SUM(nps_buyerchurn) AS nps_buyerchurn,
		    SUM(nps_sellerchurn) AS nps_sellerchurn
		FROM
		    monthly_nps_pivot
		GROUP by 1,2

)
       select pc.full_date,pc.country_iso
,sales_leads
,gs.gross_sales,cs.cancelled_sales
,nvl(gs.gross_sales,0) - nvl(cs.cancelled_sales,0) as net_sales
,nvl(gsf.gross_sales,0) - nvl(csf.cancelled_sales,0) as net_sales_f
,trunc(net_sales_f::float / nullif(net_sales::float,0),4) "%_fin"
,purchase_lead,purchase,inspection_made
,bookings_fin + bookings_cash as total_bookings,total_returned
,inspection_scheduled,inspection_approved
,bookings_fin,bookings_cash
,inv.ready_to_sell,inv.total_inventory
--,imb.ready_to_sell,imb.total_inventory
,day_is_first_of_month,day_is_last_of_month,week_day_number,year_week_number,year_number,month_number 
,nps_buyer,nps_seller,nps_aftersales,nps_buyerchurn,nps_sellerchurn
,purchase_c2b
,d.gross_delivery,d."return",coalesce(d.gross_delivery,0)-coalesce(d."return",0) AS net_delivery_mx
,da.gross_delivery,da."return",coalesce(da.gross_delivery,0)-coalesce(da."return",0) AS net_delivery_all
,da.gross_delivery_f,da."return_f",coalesce(da.gross_delivery_f,0)-coalesce(da."return_f",0) AS net_fin_delivery
,case when pc.country_iso='MX' then net_delivery_mx else net_delivery_all end deliveries_total

from period_country as pc	
	left join sales_lead as sl on sl.full_date=pc.full_date and sl.country_iso=pc.country_iso
	left join gross_sales as gs on gs.full_date=pc.full_date and gs.country_iso=pc.country_iso
	left join cancelled_sales as cs on cs.full_date=pc.full_date and cs.country_iso=pc.country_iso
	left join gross_sales_fin as gsf on gsf.full_date=pc.full_date and gsf.country_iso=pc.country_iso
	left join cancelled_sales_fin as csf on csf.full_date=pc.full_date and csf.country_iso=pc.country_iso
	left join purchase as p on p.full_date=pc.full_date and p.country_iso=pc.country_iso
	left join bookings_fin as bkf on bkf.full_date=pc.full_date and bkf.country_iso=pc.country_iso
	left join bookings_cash as bkc on bkc.full_date=pc.full_date and bkc.country_iso=pc.country_iso
	left join returned as r on r.full_date=pc.full_date and r.country_iso=pc.country_iso
	left join inventory as inv on inv.full_date=pc.full_date and inv.country_iso=pc.country_iso
	left join nps as nps on nps.full_date=pc.full_date and nps.country_iso=pc.country_iso
	left join deliveries as d on d.full_date=pc.full_date and d.country_iso=pc.country_iso
	left join deliveries_all as da on da.full_date=pc.full_date and da.country_iso=pc.country_iso
--	left join inv_br as ibr on ibr.full_date=pc.full_date and ibr.country_iso=pc.country_iso

order by 1,2 

