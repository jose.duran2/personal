with base as
(select 
--row_number() over (partition by at2.pers_person_pin||at2.att_date order by 1 desc) as rn,
at2.auth_area_name,
at2.auth_area_no,
at2.pers_person_pin, 
at2.pers_person_name,
at2.pers_person_last_name,
at2.auth_dept_code,
at2.auth_dept_name,
at2.att_date,
at2.att_datetime,
at2.att_state,
--att_time,
--att_verify,
at2.device_id,
at2.device_name,
at2.device_sn,
per.wo_id
from att_transaction at2 
left join (select
pc.person_id,
pc.person_pin,
pae.attr_value6 as wo_id
from pers_card pc
left join pers_attribute_ext pae on pae.person_id = pc.person_id) per on per.person_pin = at2.pers_person_pin  
-- where at2.auth_area_name = 'MEXICO'
--and at2.pers_person_pin in ('4581') 
order by 11 desc)
select 
b.auth_area_name,
b.auth_area_no,
b.pers_person_pin, 
b.pers_person_name,
b.pers_person_last_name,
b.auth_dept_code,
b.auth_dept_name,
to_date(b.att_date ,'yyyy-mm-dd'),
min(b.att_datetime) as first_in,
max(b.att_datetime) as last_in,
max(b.att_datetime) - min(b.att_datetime) as work_time,
--b.att_state,
det.card_count,
det.att_times,
--att_time,
--att_verify,
b.device_id,
b.device_name,
null,
b.device_sn,
b.wo_id
from base b
left join att_day_card_detail det on det.pers_person_pin||det.att_date = b.pers_person_pin||b.att_date
where to_date(b.att_date, 'YYYY-MM-DD') between '2023-03-01' and current_date
group by 1,2,3,4,5,6,7,8,12,13,14,15,16,17,18
union
select 
null,
null,
pin,
"name",
last_name,
dept_code,
dept_name,
date(first_in_time) as grabar_fecha, 
first_in_time as first_in_time,
last_out_time as last_out_time,
last_out_time - first_in_time as work_time,
null,
null,
null,
reader_name_in, 
reader_name_out,
null,
per.wo_id
from acc_firstin_lastout afl 
left join (select
pc.person_id,
pc.person_pin,
pae.attr_value6 as wo_id
from pers_card pc
left join pers_attribute_ext pae on pae.person_id = pc.person_id) per on per.person_pin = afl.pin   
--where pin in ('4581')
;
