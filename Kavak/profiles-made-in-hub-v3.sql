/*

step 1
	-> pedir acceso a olimpo_postgres_global_landing -> Todos
	-> probar con la fuente en refined que demos el mismo resultado -> Peter
	-> armar un job y disponibilizar el resultado de esta query en servig ->
step 2

pasar a refined si no existe
	->financing_db_financing_profile -> Sofi
	->financing_db_financing_financial_user -> Sofi
	->financing_db_financing_credit_request -> Sofi
*/


with users as (select 
					u.use_id,
					u.use_email
					from (select
							max(us.dms_date) as us_dms_date,
							us.use_id
							from olimpo_postgres_global_landing.user_db_user_user us
							group by us.use_id) as us_dates
					left join olimpo_postgres_global_landing.user_db_user_user u
					on u.use_id = us_dates.use_id and u.dms_date = us_dates.us_dms_date),-- usaria rn para mejorar performance
	financial_user as (select 
							fu.fus_id as fus_id, 
							fu.fus_user_id as fus_user_id, 
							fu.fus_financial_data as fus_financial_data,
							json_extract_path_text(fu.fus_personal_data,'phoneNumber') as user_phone_number
							from (SELECT
									max(fu.dms_date) as fu_dms_date, 
									fu.fus_id
									from olimpo_postgres_global_landing.financing_db_financing_financial_user fu
									group by fu.fus_id) as fu_dates
							left join olimpo_postgres_global_landing.financing_db_financing_financial_user fu
								on fu.fus_id = fu_dates.fus_id and fu.dms_date = fu_dates.fu_dms_date),
	profile as (select 
					pr.prf_id,
					pr.prf_fus_id,
					pr.prf_final_status,
					pr.prf_source,
					pr.prf_creation_date
					from (select 
							max(pr.dms_date) as prf_dms_date,
							pr.prf_id
							from olimpo_postgres_global_landing.financing_db_financing_profile pr
							group by pr.prf_id) as prf_dates
					left join olimpo_postgres_global_landing.financing_db_financing_profile pr
					on pr.prf_id = prf_dates.prf_id and pr.dms_date = prf_dates.prf_dms_date
					where pr.prf_creation_date BETWEEN CURRENT_DATE - 1 + '11:00:00.000000' :: TIME AND CURRENT_DATE + '11:00:00.000000' :: TIME),
	credit_request as (select 
						crq.crq_id,
						crq.crq_fus_id,
						crq.crq_credit_context
						from (select 
								max(crq.dms_date) as crq_dms_date, 
								crq.crq_id
								from olimpo_postgres_global_landing.financing_db_financing_credit_request crq
								group by crq.crq_id) as crq_unique
						left join olimpo_postgres_global_landing.financing_db_financing_credit_request crq on 
							crq.crq_id = crq_unique.crq_id and crq.dms_date = crq_unique.crq_dms_date
						where crq.crq_update_date > CURRENT_DATE - 1),
	simulation as (select 
					smn.*
					from (SELECT
							max(smn.dms_date) as smn_dms_date,
							smn.smn_id
							from olimpo_postgres_global_landing.financing_db_financing_simulation smn
							group by smn.smn_id) as smn_unique
					left join olimpo_postgres_global_landing.financing_db_financing_simulation smn 
					on smn.smn_id = smn_unique.smn_id and smn.dms_date = smn_unique.smn_dms_date
					where smn.smn_creation_date > CURRENT_DATE - 1),
	unique_hubs as (select hub.*
					from (select 
							max(hub.dms_date) as hub_dms_date,
							hub.hub_id
							from olimpo_postgres_global_landing.financing_db_financing_hub hub
							group by hub.hub_id) as hub_unique
					LEFT join olimpo_postgres_global_landing.financing_db_financing_hub hub
					on hub.hub_id = hub_unique.hub_id and hub.dms_date = hub_unique.hub_dms_date),
	unique_profile_hubs as (select prh.*
							from (select max(prh.dms_date) as prh_dms_date,
										prh.prh_id
										from olimpo_postgres_global_landing.financing_db_financing_profile_hub prh
										group by prh.prh_id) as prh_unique
							left join olimpo_postgres_global_landing.financing_db_financing_profile_hub prh
							on prh.prh_id = prh_unique.prh_id and prh.dms_date = prh_unique.prh_dms_date
							where prh.prh_creation_date BETWEEN CURRENT_DATE - 1 + '11:00:00.000000' :: TIME AND CURRENT_DATE + '11:00:00.000000' :: TIME),
	hubs AS (SELECT 
				h.hub_name,
		 		ph.prh_prf_id 
			 FROM unique_profile_hubs ph  
			 INNER JOIN unique_hubs h ON h.hub_id = ph.prh_hub_id)
select
	distinct
	financialUser.fus_user_id AS user_id,
	users.use_email as user_email,
	profile.prf_final_status AS final_status,
    profile.prf_creation_date AS profile_creation_date,
    profile.prf_source,
    case when profile.prf_source = 'APOLO' THEN json_extract_path_text(creditRequest.crq_credit_context, 'KIKOYA', 'folioId') 
    	else json_extract_path_text(financialUser.fus_financial_data, 'KIKOYA', 'folioId') end AS folio_id,
    h.hub_name,
    simulationTable.*
FROM profile PROFILE
INNER JOIN financial_user financialUser ON financialUser.fus_id = profile.prf_fus_id
inner join users users on users.use_id = financialUser.fus_user_id
INNER JOIN hubs h on h.prh_prf_id = profile.prf_id
left join credit_request creditRequest on creditRequest.crq_fus_id = financialUser.fus_id
LEFT JOIN
	  (SELECT simulation.smn_crq_id,
	  		  simulation.smn_minimum_downpayment AS minimumDownPayment,
	          simulation.smn_selected_downpayment AS selectedDownPayment,
	          simulation.smn_installments AS installments,
	          simulation.smn_installment_value AS installmentValue,
	          simulation.smn_simulation_origin AS simulationOrigin,
	          simulation.smn_insurance AS insurance,
	          simulation.smn_assessor AS assessor,
	          simulation.smn_category AS category,
	          simulation.smn_cou_id AS countryId,
	          simulation.smn_kavak_total_payment_method AS kavakTotalPaymentMethod,
	          simulation.smn_insurance_payment_method AS insurancePaymentMethod,
	          simulation.smn_user_email AS userEmail,
	          simulation.smn_update_date AS simulationUpdateDate,
	          simulation.smn_delete_date AS simulationDeleteDate
	   FROM simulation simulation
	   WHERE simulation.smn_cou_id = 484) simulationTable
ON simulationTable.smn_crq_id = creditRequest.crq_id
	   		AND Extract(hour FROM (simulationTable.simulationUpdateDate - profile.prf_creation_date)) <= 2
	   		and Extract(minute FROM (simulationTable.simulationUpdateDate - profile.prf_creation_date)) <= 120
	   		and Extract(day FROM (simulationTable.simulationUpdateDate - profile.prf_creation_date)) <= 0
ORDER BY profile.prf_creation_date asc;