DROP TABLE IF EXISTS users_op_viewed;
create local temporary table users_op_viewed as
	select distinct user_id from amplitude_global_refined.amplitude_events 
	where lower(user_id) != 'none' AND event_type = 'profilingOpportunity_viewed' and client_event_time::date >= (current_date-60);

DROP TABLE IF EXISTS df_union;
create local temporary table df_union as
with 
/*users_op_viewed as (
select distinct user_id from amplitude_global_refined.amplitude_events where client_event_time::date >= (current_date-5)
),
*/
users_af as (
	select distinct u.user_id
	,case 
		when event_type = 'profilingFunnel_started' and event_properties like  '%Auto First Banner%' then 'boton_azul'
		when event_type = 'profilingFunnel_started' and event_properties like  '%Auto First%' then 'banner_vip'
	else 'none' end "source"
	,max(client_event_time) "date"
	from amplitude_global_refined.amplitude_events ae
		inner join users_op_viewed u on ae.user_id = u.user_id
	where 1=1
	and "source" !='none'
	group by 1,2
) 
, users_ff_banner_S01 as (
	
	select ae.user_id,max(ae.client_event_time) "date"
	from amplitude_global_refined.amplitude_events ae
		inner join users_op_viewed u on ae.user_id = u.user_id
	where 1=1
	and ae.event_type = 'catalogFFBanner_selected'
	--and ae.user_id= 3030658
	group by 1
) 
, users_ff_banner_S02 as (
	
	select ae.user_id,MIN(ae.client_event_time) as date_event,u."date",datediff(second,"date",date_event) as "delayed"
	from amplitude_global_refined.amplitude_events ae
		inner join users_ff_banner_S01 u on ae.user_id = u.user_id and ae.client_event_time > u."date"
	where 1=1
	and ae.event_type = 'profilingLandingPage_viewed'
	--and ae.user_id= 3030658
	group by 1,3
	
) 
, users_ff_prof_S01 as (
	
	select u.user_id,max(client_event_time) "date"
	from amplitude_global_refined.amplitude_events ae
		inner join users_op_viewed u on ae.user_id = u.user_id
	where 1=1
	and event_type = 'profilingLandingPage_viewed'
	group by 1
) 
,users_ff_prof_S02 as (
	
	select ae.user_id,MIN(ae.client_event_time) as date_event,u."date",datediff(second,"date",date_event) as "delayed"
	from amplitude_global_refined.amplitude_events ae
		right outer join users_ff_prof_S01 u on ae.user_id = u.user_id and ae.client_event_time > u."date"
	where 1=1
	and event_type = 'catalogFFBanner_selected'
	group by 1,3
) 
, hubs_normal_flow as (

	select distinct u.user_id,max(client_event_time) "date"
	from amplitude_global_refined.amplitude_events ae
		inner join users_op_viewed u on ae.user_id = u.user_id
	where 1=1
	and event_properties like  '%/mx/hub-pago-a-meses-oportunidad%'
	group by 1
) 
, minerva as (
	select * from (
		select prf_fus_id user_id,prf_final_status as profile,prf_creation_date as "date"
		, ROW_NUMBER() over (partition by prf_fus_id order by prf_creation_date desc) rn
		from olimpo_postgres_global_refined.financing_db_financing_profile
		where 1=1 
		and prf_creation_date::date >= (current_date - 60)
		--and prf_fus_id=55715
	) where rn=1
)
,last_booking as (

select dc.bk_client_olimpo user_id,fb.bk_booking,DATE(fb.fk_booking_creation_date) booking_creation_date
,row_number () over (partition by dc.bk_client order by fb.fk_booking_creation_date desc) rn_booking
from dwh.fact_booking fb 
	left join dwh.dim_client dc on dc.sk_client = fb.fk_client and dc.is_active
--where user_id in (55715)
)
--, df_union as (

-- REVISAR SI SE QUIERE PRIORIZAR ALGUNA ENTRADA, EN ESE CASO HAY QUE USAR SOLO "UNION" Y NO "UNION ALL"
select 			 user_id::text,"date",null date_event,null delayed,'N/A' as "profile", 'users_af' as "source" from users_af
union all select user_id::text,"date",date_event	 ,delayed	  ,'N/A' as "profile", 'ff_banner' as "source" from users_ff_banner_S02
union all select user_id::text,"date",date_event	 ,delayed	  ,'N/A' as "profile", 'ff_prof' as "source" from users_ff_prof_S02
union all select user_id::text,"date",null date_event,null delayed,'N/A' as "profile", 'hubs' as "source" from hubs_normal_flow
union all select user_id::text,"date",null date_event,null delayed,			 profile, 'minerva' as "source" from minerva

--) 

----final statement 
select

--ROW_NUMBER() over (partition by user_id,"source" order by "date") fcy_order
row_number() over(partition by user_id||"source" order by "date" desc) as rn
,df.*
--,u.user_id as "review"
,fb.bk_booking,dp."type" as payment_type,fb.flag_main_booking
, DATE(fb.fk_booking_creation_date) as booking_creation_date, DATE(fb.fk_delivery_date) as "sold_date"
,case when fb.bk_booking is not null then true end as "flag_booking", fb.flag_delivered as "flag_sold"
,(DATE(fb.fk_booking_creation_date) - DATE("date")) as "flag_booking_true"
,ROW_NUMBER() over (partition by user_id order by DATE(fb.fk_booking_creation_date) desc) fcy_booking
,fb.amt_main_loan_downpayment,fb.amt_main_loan_payment,fb.amt_main_first_payment

from df_union as df
		
	-- full outer join users_op_viewed u on u.user_id=df.user_id
	left join last_booking lb on lb.user_id = df.user_id
	left join dwh.fact_booking fb on lb.booking_id = fb.booking_id --and fb.flag_main_booking
	left join dwh.dim_payment_type dp on dp.sk_payment_type = fb.fk_payment_type
	
--where user_id in (55715)

--order by rn asc
