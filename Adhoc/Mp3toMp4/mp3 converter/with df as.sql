with df as 

(with funnel_kikoya as 
	(with kikoya as 
		(SELECT
		    product_app_id AS folio_id,
		    project_id,
		    decorated_folio,
		    product_app_created_at AS product_application_created_at,
		    product_app_updated_at AS product_application_updated_at,
		    arbol_id,
		    arbol_updated_at,
		    email,
		    NULLIF(customer_profile, 'N/A') AS customer_profile,
		    NULLIF(data_alerts, 'N/A') AS data_alerts,
		    CAST(CONVERT_TIMEZONE('America/Mexico_City', product_app_created_at) AS DATE) AS summary_creation_date,
		    CASE WHEN customer_profile is NULL THEN NULL ELSE SPLIT_PART(customer_profile, '_', 1) END AS split_calif,
		    CASE WHEN customer_profile is NULL THEN FALSE ELSE TRUE END AS finished_profiling,
		    CASE WHEN customer_profile is NULL THEN NULL ELSE
		        CASE WHEN RIGHT(customer_profile, 2) IN('AX', 'PX') THEN RIGHT(customer_profile, 2)
		        ELSE RIGHT(customer_profile, 1)
		        END
		    END AS sufix
		FROM serving.kikoya_profiling_folios_report kpflr
		WHERE 1=1
		    --AND CONVERT_TIMEZONE('America/Mexico_City', 'UTC', product_app_created_at) >= CAST('2021-09-01' AS TIMESTAMP)
		    AND email NOT ILIKE '%prueba%'
		    AND email NOT ILIKE '%test%'
		    and summary_creation_date = '2023-03-01'),
		 flags_type as  (select folio_id, flag ,
			sum(distinct(case when flag = 'Flag_Edad' or flag = 'Flag_Tipo_Asenta' or flag = 'Flag_MOP96' or flag = 'Flag_MOP97' or flag = 'Flag_MOP99' or flag = 'Flag_Fraude'  or flag = 'MOP4_40%' or flag = 'MOP3_60%' or flag = 'Flag_R_Consultas' or flag = 'Flag_Residencia' or flag = 'Flag_Zona' or flag = 'Flag_Sin_Cob' or flag = 'Flag_CobyZona' or flag = 'Flag_Endeudamiento' or flag = 'Flag_PLD' or flag = 'Flag_PEP' or flag = 'FLAG_5/6/7' or  flag = 'Flag_MOPs_Combinados_45%'  or flag = 'MOP567_20%' or flag = 'Flag_Financiamiento_Vigente' then 1 else 0 end)) AS hard_flags,
			sum(distinct(case when flag = ' Flag_Mayor35SinBuro' or  flag = 'Flag_EdadSinBuro' or flag = 'Flag_BC_Score' or flag = 'Flag_CapPago' then 1 else 0 END)) AS non_hard_flags,
			sum(distinct(case when flag = 'Flag_RFC' or flag = 'Flag_Error_CP' then 1 else 0 END )) error_flags
		FROM (
		  SELECT product_app_id as folio_id, SPLIT_PART(data_alerts, ' ', n.n) AS flag
		  FROM serving.kikoya_profiling_folios_report kpfr
		  CROSS JOIN (SELECT 1 n UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6
					              UNION SELECT 7 UNION SELECT 8 UNION SELECT 9 UNION SELECT 10 UNION SELECT 11) n
					) subquery-- where flag is not null and flag <> ''
			group by 1,2)
			select kikoya.*, flag, hard_flags, non_hard_flags, error_flags from kikoya left join flags_type on kikoya.folio_id = flags_type.folio_id
),

olimpo as 
		(SELECT
		json_extract_path_text(fus_financial_data, 'KIKOYA', 'folioId') AS folio_id,
		  fus.fus_country_code AS country_code
		FROM  olimpo_postgres_global_refined.financing_db_financing_financial_user fus
		WHERE fus_financial_data::text LIKE '%KIKOYA%'
		UNION
		SELECT DISTINCT
		json_extract_path_text(crq_credit_context, 'KIKOYA', 'folioId') AS folio_id,
		  CASE
		    WHEN
		    folio_id in ('618993', '848914') THEN 'MX'
		    ELSE fus_country_code
		  END AS country_code
		FROM olimpo_postgres_global_refined.financing_db_financing_credit_request crq
		INNER JOIN olimpo_postgres_global_refined.financing_db_financing_financial_user fus ON crq.crq_fus_id = fus.fus_id

)

select  funnel_kikoya.folio_id,
finished_profiling,
flag,
sufix,
summary_creation_date,
  COALESCE(LAST_VALUE(country_code IGNORE NULLS) OVER (PARTITION BY email), 'UNKNOWN') AS summary_country_code,
CASE WHEN sufix = 'R' THEN CONCAT(SUBSTRING(split_calif, 1, 1), '_R') ELSE SUBSTRING(split_calif, 1, 1) END AS calif_to_agg,
SUBSTRING(split_calif, 1, 1) AS perfil,
  EXTRACT(YEAR FROM summary_creation_date)::varchar || '-' || LPAD(EXTRACT(WEEK FROM summary_creation_date)::varchar, 2, '0') AS week_string,
   hard_flags,
   non_hard_flags,
   error_flags
from funnel_kikoya left join olimpo on funnel_kikoya.folio_id = olimpo.folio_id)
(SELECT summary_creation_date,
  folio_id, finished_profiling, sufix, calif_to_agg,  summary_country_code,
  COUNT(DISTINCT CASE WHEN finished_profiling is true THEN folio_id ELSE NULL END) AS terminan_perfilamiento,
  COUNT(DISTINCT CASE WHEN finished_profiling is false THEN folio_id ELSE NULL END) AS inician_pero_no_terminan,
  COUNT(DISTINCT CASE WHEN finished_profiling is true or finished_profiling is false THEN folio_id ELSE NULL END) AS inician_perfilamiento,
  
  COUNT(DISTINCT CASE WHEN finished_profiling = true AND hard_flags > 0 THEN folio_id ELSE NULL END) as rechazos_duros,
  max(CASE when finished_profiling AND non_hard_flags = 1 THEN count ELSE NULL END) recha_no_duro,

  max(CASE WHEN flag = 'Flag_Edad' THEN count ELSE 0 END) AS "rechazo_solo_por_edad",
  MAX(CASE WHEN flag = 'Flag_Tipo_Asenta' THEN count ELSE 0 END) AS "rechazo_solo_por_tipo_asenta",
  MAX(CASE WHEN flag = 'Flag_MOP96' THEN count ELSE 0 END) AS "rechazo_solo_por_mop96",
  MAX(CASE WHEN flag = 'Flag_MOP97' THEN count ELSE 0 END) AS "rechazo_solo_por_mop97",
  MAX(CASE WHEN flag = 'Flag_MOP99' THEN count ELSE 0 END) AS "rechazo_solo_por_mop99",
  MAX(CASE WHEN flag = 'Flag_Fraude' THEN count ELSE 0 END) AS "rechazo_solo_por_fraude",
  MAX(CASE WHEN flag = 'MOP4_40%' THEN count ELSE 0 END) AS "rechazo_solo_por_mop4_40%",
  MAX(CASE WHEN flag = 'Flag_R_Consultas' THEN count ELSE 0 END) AS "rechazo_solo_por_consultas",
  MAX(CASE WHEN flag = 'Flag_Residencia' THEN count ELSE 0 END) AS "rechazo_solo_por_residencia",
  MAX(CASE WHEN flag = 'Flag_Zona' THEN count ELSE 0 END) AS "rechazo_solo_por_zona",
  MAX(CASE WHEN flag = 'Flag_Sin_Cob' THEN count ELSE 0 END) AS "rechazo_solo_por_sin_cob",
  MAX(CASE WHEN flag = 'Flag_CobyZona' THEN count ELSE 0 END) AS "rechazo_solo_por_cobyzona",
  MAX(CASE WHEN flag = 'Flag_Endeudamiento' THEN count ELSE 0 END) AS "Rechazo solo por endeudamiento",
  MAX(CASE WHEN flag = 'Flag_PLD' THEN count ELSE 0 END) AS "rechazo_solo_por_PLD",
  MAX(CASE WHEN flag = 'Flag_PEP' THEN count ELSE 0 END) AS "rechazo_solo_por_PEP",
  MAX(CASE WHEN flag = 'FLAG_5/6/7' THEN count ELSE 0 END) AS "rechazo_solo_por_BC_score_5/6/7",
  MAX(CASE WHEN flag = 'Flag_MOPs_Combinados_45%' THEN count ELSE 0 END) AS "rechazo_solo_por_MOPs_Combinados_45%",
  MAX(CASE WHEN flag = 'MOP567_20%' THEN count ELSE 0 END) AS "rechazo_solo_por_MOPs_567_20%",
  MAX(CASE WHEN flag = 'Flag_Financiamiento_Vigente' THEN count ELSE 0 END) AS "rechazo_solo_por_financiamiento_vigente",
  MAX(CASE WHEN flag = 'fuera_matriz' THEN count ELSE 0 END) AS "rechazo_fuera_de_matriz_de_calificacion",
  MAX(CASE WHEN flag = 'multiples_flags' THEN count ELSE 0 END) AS "rechazo_multiples_flags",
  MAX(CASE WHEN flag = 'Flag_CapPago' THEN count ELSE 0 END) AS "rechazo_solo_por_CP",
  MAX(CASE WHEN flag = 'Flag_BC_Score' THEN count ELSE 0 END) AS "rechazo_solo_por_BC_score",
  MAX(CASE WHEN flag = 'Flag_Mayor35SinBuro' THEN count ELSE 0 END) AS "rechazo_solo_por_edad_SB",
  MAX(CASE WHEN flag = 'multiples_flags' THEN count ELSE 0 END) AS "rechazos_no_duros_multiples_flags",
  MAX(CASE WHEN flag = 'errores' THEN count ELSE 0 END) AS "solo_flags_errores",
  MAX(CASE WHEN flag = 'Flag_Error_CP' THEN count ELSE 0 END) AS "solo_Flag_error_CP",
  MAX(CASE WHEN flag = 'Flag_RFC' THEN count ELSE 0 END) AS "Solo_Flag_RFC",
  MAX(CASE WHEN flag = 'multiples_errores' THEN count ELSE 0 END) AS "multiples_flags_de_error",
  MAX(CASE WHEN calif_to_agg = 'D' THEN count ELSE 0 END) AS D,
    MAX(CASE WHEN calif_to_agg = 'Z_R' THEN count ELSE 0 END) AS Z_R,
    MAX(CASE WHEN calif_to_agg = 'A_R' THEN count ELSE 0 END) AS A_R,
    MAX(CASE WHEN calif_to_agg = 'C_R' THEN count ELSE 0 END) AS C_R,
    MAX(CASE WHEN calif_to_agg = 'F_R' THEN count ELSE 0 END) AS F_R,
    MAX(CASE WHEN calif_to_agg = 'B_R' THEN count ELSE 0 END) AS B_R,
    MAX(CASE WHEN calif_to_agg = 'E' THEN count ELSE 0 END) AS calif_to_agg_E,
    MAX(CASE WHEN calif_to_agg = 'A' THEN count ELSE 0 END) AS calif_to_agg_A,
      MAX(CASE WHEN calif_to_agg = 'B' THEN count ELSE 0 END) AS calif_to_agg_B,
  MAX(CASE WHEN calif_to_agg = 'C' THEN count ELSE 0 END) AS calif_to_agg_C,
  MAX(CASE WHEN calif_to_agg = 'D' THEN count ELSE 0 END) AS calif_to_agg_D,
  MAX(CASE WHEN calif_to_agg = 'F' THEN count ELSE 0 END) AS calif_to_agg_F,
    MAX(CASE WHEN calif_to_agg = 'D_R' THEN count ELSE 0 END) AS D_R,
    MAX(CASE WHEN calif_to_agg = 'E_R' THEN count ELSE 0 END) AS E_R,
  MAX(CASE WHEN sufix = 'A' THEN count ELSE 0 END) AS A,
  MAX(CASE WHEN sufix = 'AX' THEN count ELSE 0 END) AS AX,
  MAX(CASE WHEN sufix = 'P' THEN count ELSE 0 END) AS P,
  MAX(CASE WHEN sufix = 'PX' THEN count ELSE 0 END) AS PX,
  MAX(CASE WHEN sufix = 'R' THEN count ELSE 0 END) AS R,
  MAX(CASE WHEN sufix = 'E' THEN count ELSE 0 END) AS E,
  SUM(CASE WHEN sufix IN ('A', 'AX', 'P', 'PX', 'R', 'E') THEN count ELSE 0 END) AS total
  
FROM (
  SELECT DISTINCT folio_id,
 summary_creation_date,
 finished_profiling,
 sufix,
 summary_country_code,
  hard_flags,
   non_hard_flags,
   flag,
   error_flags, calif_to_agg,
    COUNT(DISTINCT folio_id) AS count
  FROM df
  GROUP BY 1,2,3,4,5,6,7,8,9,10
) 
sub 
where 1=1
--and folio_id = 2625611
and summary_creation_date = '2023-03-01'
GROUP BY 1, 2, 3, 4, 5, 6) --  limit 1000